README

Resume for Corbin Weidner.

The document is written in LaTeX and converted into
PDF form. The current version of the document is in the "master" branch under
the name "Resume-CorbinWeidner.pdf" in the top level directory.
